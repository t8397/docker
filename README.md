## Deployment guide

### Clone source code
```
git clone https://gitlab.com/t8397/docker.git --recursive
```

### Create .env file
```
cp .env.example .env
```

```
cp FE/.env.example FE/.env
```

### Run stack
```
docker-compose up -d
```

### Default services
- Frontend Dashboard: http://localhost:3000/dashboard
- Frontend Client: http://localhost:3000/client
- Backend Playground: http://localhost:3001/graphql
